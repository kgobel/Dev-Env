exec { 'apt-update': 
  command => '/usr/bin/apt-get update'
}

# install nodejs, npm, pip
package { ['nodejs', 'npm', 'python-pip']:
  require => Exec['apt-update'],
  ensure => installed,
}


